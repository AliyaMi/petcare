let preprocessor = 'sass'; 
const { src, dest, parallel, series, watch } = require('gulp'); 
const browserSync = require('browser-sync').create(); 
const sass = require('gulp-sass')(require('sass')); 

function browsersync() { 
  browserSync.init({ // Инициализация Browsersync 
    server: { baseDir: './' }, // Указываем папку сервера 
    notify: false, // Отключаем уведомления 
    online: true // Режим работы: true или false 
  }); 
}

function compileSass() { 
  return src('./src/scss/style.scss') 
    .pipe(sass()) 
    .pipe(dest('./css')) 
}
function watchFiles() { 
	watch('./src/scss/style.scss', series(compileSass)).on('change', browserSync.reload); 
  }


exports.browsersync = browsersync;
exports.compileSass = compileSass;
exports.watchFiles = watchFiles;

exports.default = parallel(browsersync, compileSass, watchFiles);

